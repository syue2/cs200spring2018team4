package project4;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Class {@code ProviderReport} creates a report for a provider
 * @author Michael Oram
 */
public class ProviderReport {
	private ArrayList<String> providerFiles;
	
	/**
	 * Constructor {@code ProviderReport} initializes an array of provider files.
	 */
	public ProviderReport(){
		providerFiles = new ArrayList<String>();
	}
	
	/**
	 * Method {@code createProviderReport(String,String,String,ArrayList<String>,String,int,double)} creates a provider report.
	 * @param providerName		the provider's name
	 * @param providerNumber	the provider's password
	 * @param providerAddress	the provider's address
	 * @param serviceInfo		info on the service provider
	 * @param currentDate		the current date
	 * @param consult			consultations
	 * @param totalFee			the total cost
	 * @return Nothing.
	 * @throws IOException
	 */
	public void createProviderReport(String providerName, String providerNumber, String providerAddress, ArrayList<String> serviceInfo, String currentDate, int consult, double totalFee) throws IOException {	// Implement constructor???
		Path providerFile = Paths.get("provider_" + providerName + currentDate + ".txt");
		providerFiles.add("provider_" + providerName + currentDate + ".txt");
		ArrayList<String> data = new ArrayList<String>();
		int i;
		data.add(providerName);
		data.add("Provider Number: " + providerNumber);
		data.add("Provider Address: " + providerAddress);
		data.add("\n");
		data.add("Services Provided");
		data.add("\n");
		for (i = 0; i < serviceInfo.size(); i++){
			data.add(serviceInfo.get(i));
		}
		data.add("\n");
		data.add("Total Consultations: " + String.valueOf(consult));
		if (totalFee > 99999.99) {
			totalFee = 99999.99;
		}
		data.add("Total Fee: " + String.valueOf(totalFee));
		Files.write(providerFile, data);
		
		return;
	}
	
	/**
	 * Method {@code deleteFiles()} deletes the files.
	 * @return Nothing.
	 * @throws IOException
	 */
	public void deleteFiles() throws IOException {
		Path files;
		int i;
		for (i = 0; i < providerFiles.size();i++) {
			files = Paths.get(providerFiles.get(i));
			Files.delete(files);
		}
		return;
	}
	
}