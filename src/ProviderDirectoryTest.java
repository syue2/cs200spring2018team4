package project4; 

import static org.junit.Assert.*;

import org.junit.Test;

import project4.ProviderDirectory;

public class ProviderDirectoryTest {

	@Test
	public void testGetServiceName() {		//Test Failure
		ProviderDirectory proDirectory = new ProviderDirectory();
		assertEquals("Invalid",proDirectory.getServiceName("383858"));
	}

	@Test //Test Success
	public void testGetServiceFee() {
		ProviderDirectory proDirectory = new ProviderDirectory();
		assertEquals(52.7, proDirectory.getServiceFee("314314"), 3);
	}
	@Test //Test Success
	public void testGetServiceCode() {
		ProviderDirectory proDirectory = new ProviderDirectory();
		assertEquals("598470",proDirectory.getServiceCode("Dietician Session"));
	}
}
