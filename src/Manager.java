package project4;
/**
 * Class {@code Manager} holds data of individual manager
 * @author James Tubbs
 */
public class Manager {
	private String number;
	private String name;
	private String address;
	
	/**
	 * Constructor {@code Manager(String,String,String)} uses its parameters to make a new {@code Manager}.
	 * @param number  to be assigned to number attribute
	 * @param name    to be assigned to name attribute
	 * @param address to be assigned to address attribute
	 */
	public Manager(String managerNumber, String managerName, String managerAddress) {
		number = managerNumber;
		name = managerName;
		address = managerAddress;
	}
	
	/**
	 * Getter method {@code getManagerName()} retrieves attribute {@code name}.
	 * @return String returns name attribute
	 */
	public String getManagerName() {
		return name;
	}
	
	/**
	 * Getter method {@code getManagerNumber()} retrieves attribute {@code number}.
	 * @return String returns number attribute
	 */
	public String getManagerNumber() {
		return number;
	}
	
	/**
	 * Getter method {@code getManagerAddress()} retrieves attribute {@code address}.
	 * @return String returns address attribute
	 */
	public String getManagerAddress() {
		return address;
	}
}