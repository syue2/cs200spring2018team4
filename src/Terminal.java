package project4;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
//import java.util.Calendar;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.IOException;


/**
 * class (@code Terminal) supports the user interface for the Chocoholics Anonymous system. 
 * @author James Tubbs
 */
public class Terminal {
	public static final Scanner input = new Scanner(System.in);
	public static final ProviderRecord proRecord =  new ProviderRecord();
	public static final MemberRecord memRecord = new MemberRecord();
	public static final ProviderDirectory proDirectory = new ProviderDirectory();
	public static final ServiceRecord servRecord = new ServiceRecord();
	public static final OperatorRecord opeRecord = new OperatorRecord();
	public static final ManagerRecord manaRecord = new ManagerRecord();
	public static final EFT EFTRecord = new EFT();
	public static final SummaryReport sumReport = new SummaryReport();
	public static final ProviderReport proReport = new ProviderReport();
	public static final MemberReport memReport = new MemberReport();

	/**
	 * Method {@code main(String[])} is launched when program is called.
	 * @param args parameters included in running of program
	 * @return Nothing.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		System.out.println("\nChocAn Terminal\n");
		System.out.println("Please enter your choice value to login...\n\n1. Provider Login\n2. Operator Login\n3. Manager Login\nEnter 0 to reset the system.\n");
		String providerNumber = "";
		String operatorNumber = "";
		String memberNumber = "";
		String managerNumber = "";
		
		boolean match;
		int choice;
		manaRecord.addManager();
		choice = Integer.valueOf(input.nextLine());
		
		while (choice != -2) {
			switch (choice) { 
			 	case 0:
			 		System.out.println("Resetting system. Goodbye.");
			 		deleteAllFiles();
			 		choice = -2;
			 		break;
				case 1: 	//Provider Login
					System.out.println("Please enter your ID\n");
					providerNumber = input.nextLine();
					match = proRecord.verifyProvider(providerNumber); //need to finish verifyProvider (because we must addProvider 1st)
					if (match == true) {
						System.out.println("\n\nWelcome " + proRecord.getProviderName(providerNumber) + "!");
						System.out.println("What would you like to do?\n\n1. Verify Member\n2. Bill ChocAn\n3. Request Provider Directory\nEnter 0 to logout.\n");
						choice = 0;
						choice = Integer.valueOf(input.nextLine());
						while (choice != -1) {
							switch (choice) {
								case 0:
									System.out.println("Logging out\n");
									choice = -1;
									break;
								case 1: //VERIFY MEMBER
									System.out.println("Enter the member number for verification.\n");
									memberNumber = input.nextLine();
									match = memRecord.verifyMember(memberNumber);
									if (match == true) {
									System.out.println(memRecord.getMemberName(memberNumber) + "\n");
									System.out.println("Validated to receive health care service.\n");
									}
									else {
										System.out.println("Invalid member number");
									}
									break;
								case 2: // BILL CHOCAN
									billChocAn(providerNumber);
									break;
								case 3: // Request Provider Directory
									requestProviderDirectory();
									break;
								default:
									System.out.println("Invalid input");
							}
							if (choice != -1) {
								System.out.println("\n\nWelcome " + proRecord.getProviderName(providerNumber) + "!");
								System.out.println("\n\nWhat would you like to do?\n\n1. Verify Member\n2. Bill ChocAn\n3. Request Provider Directory\nEnter 0 to logout.\n");
								choice = Integer.valueOf(input.nextLine());
							}
						}	
					}
					else {
					
						System.out.println("Invalid Provider Number");
					}
					break;
				case 2:		//Operator Login
					System.out.println("Please enter your operator number\n");
					operatorNumber = input.nextLine();
					match = opeRecord.verifyOperator(operatorNumber); // Need to add operatorRecord class
					if (match == true) {
						System.out.println("\n\nWelcome " + opeRecord.getOperatorName(operatorNumber) + "!");
						System.out.println("\nWhat would you like to do?\n\n1. Create Member\n2. Update Member\n3. Delete Member\n4. Create Provider\n5. Update Provider\n6. Delete Provider\nEnter 0 to logout.\n");
						choice = 0;
						choice = Integer.valueOf(input.nextLine());
						while (choice != -1) {
							switch (choice) {
								case 0:
									System.out.println("Logging out\n");
									choice = -1;
									break;
								case 1:	//Create Member
									manageAccount("Create", "Member");
									break;
								case 2: //Update Member
									manageAccount("Update", "Member");
									break;
								case 3: //Delete Member
									manageAccount("Delete", "Member");
									break;
								case 4: //Create Provider
									manageAccount("Create", "Provider");
									break;
								case 5: //Update Provider
									manageAccount("Update", "Provider");
									break;
								case 6: //Delete Provider
									manageAccount("Delete", "Provider");
									break;
								default:
									System.out.println("Invalid input");
							}
							if (choice != -1) {
							System.out.println("\n\nWelcome " + opeRecord.getOperatorName(operatorNumber) + "!");
							System.out.println("\nWhat would you like to do?\n\n1. Create Member\n2. Update Member\n3. Delete Member\n4. Create Provider\n5. Update Provider\n6. Delete Provider\nEnter 0 to logout.\n");
							choice = Integer.valueOf(input.nextLine());
							}
						}			
					}
					else {
						System.out.println("Invalid operator number");
						
					}
					break;
				case 3: // MANAGER LOGIN
					System.out.println("Please enter your manager number\n");
					managerNumber = input.nextLine();
					match = manaRecord.verifyManager(managerNumber); 
					if (match == true) {
						System.out.println("\n\nWelcome " + manaRecord.getManagerName(managerNumber) + "!");
						System.out.println("\nWhat would you like to do?\n\n1. Create Operator\n2. Update Operator\n3. Delete Operator\n4. Make Member Reports\n5. Make Provider Reports\n6. Run Main Accounting Procedure\nEnter 0 to logout\n");
						choice = 0;
						choice = Integer.valueOf(input.nextLine());
						while (choice != -1) {
							switch(choice){
							case 0:
								System.out.println("Logging out\n");
								choice = -1;
								break;
							case 1: //Create Operator
								manageAccount("Create", "Operator");
								break;
							case 2: //Update Operator
								manageAccount("Update", "Operator");
								break;
							case 3: //Delete Operator
								manageAccount("Delete", "Operator");
								break;
							case 4: //Begin Member Reports
								beginMemberReports();
								break;
							case 5: //Begin Provider Reports
								beginProviderReports();
								break;
							case 6:
								beginMemberReports();
								beginProviderReports();
								System.out.println("\nMaking summary report...\n");
								beginSummaryReport();
								System.out.println("Updating EFT Record...\n");
								createEFTData();
								break;
							default:
								System.out.println("Invalid input");
							}
							if (choice != -1) {
							System.out.println("\n\nWelcome " + manaRecord.getManagerName(managerNumber) + "!");
							System.out.println("\nWhat would you like to do?\n\n1. Create Operator\n2. Update Operator\n3. Delete Operator\n4. Make Member Reports\n5. Make Provider Reports\n6. Run Main Accounting Procedure\nEnter 0 to logout\n");
							choice = Integer.valueOf(input.nextLine());
							}
						}
					}
					else {
						System.out.println("Invalid manager number");
						
					}
					break;
				default:
					System.out.println("Invalid input\n");
			}
			if (choice != -2) {
				System.out.println("\nChocAn Terminal\n");
				System.out.println("Please enter your choice value to login...\n\n1. Provider Login\n2. Operator Login\n3. Manager Login\nEnter 0 to reset the system.\n");
				choice = Integer.valueOf(input.nextLine());
			}
		}
	}
	
	/**
	 * Method {@code deleteAllFiles()} deletes all records and reports.
	 * @return Nothing
	 * @throws IOException
	 */
	public static void deleteAllFiles() throws IOException {
		memRecord.deleteFile();
		proRecord.deleteFile();
		opeRecord.deleteFile();
		manaRecord.deleteFile();
		servRecord.deleteFile();
		sumReport.deleteFile();
		proReport.deleteFiles();
 		memReport.deleteFiles();
 		EFTRecord.deleteFile();
		
		return;
	}
	
	/**
	 * Method {@code billChocAn(String)} bills ChocAn for provider's services to members.
	 * @param providerNumber the password of the provider
	 * @return Nothing.
	 * @throws IOException
	 */
	public static void billChocAn(String providerNumber) throws IOException {
		boolean match;
		String memberNumber;
		String nameOfService;
		String comments;
		String dateOfService = "";
		String serviceCode = "";
		String answer = "";
		String currentTime = "";
		String month = "";
		String day = "";
		String year = "";
		String checkServiceCode = "";
		String currentDate = "";
		String checkServiceDate = "";
		String checkMemberNumber = "";
		String memberName = "";
		String checkServiceFee;
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM-dd-yyyy hh:mm:ss");
		LocalDateTime now;
		
		System.out.println("Enter the member number for verification.\n");
		memberNumber = input.nextLine();
		match = memRecord.verifyMember(memberNumber);
		if (match == true) {
			System.out.println("Validated\n");
			System.out.println("Enter the month the service was given. (Enter in digit form. Example: March is 03.)\n"); //DATE OF SERVICE NEEDS TO BE FORMATTED SOMEHOW
			month = input.nextLine();
			while (month.length() != 2) {
				System.out.println("Error: Month input should be two characters.\n");
				System.out.println("Enter the month the service was given. (Enter in digit form. Example: March is 03)\n");
				month = input.nextLine();
			}
			System.out.println("Enter the day the service was given. (Two digit form. Example: The third day of a month is 03.)\n");
			day = input.nextLine();
			while (day.length() != 2) {
				System.out.println("Error: Day input should be two characters.\n");
				System.out.println("Enter the day the service was given.( Two digit form. Example: The third day of a month is 03.)");
				day = input.nextLine();
			}
			System.out.println("Enter the year the service was given. (Four digit form)\n");
			year = input.nextLine();
			while (year.length() != 4) {
				System.out.println("Error: Year input should be four characters.\n");
				System.out.println("Enter the year the service was given. (Four digit form)\n");
				year = input.nextLine();
			}
			dateOfService = month + "-" + day + "-" + year;
			System.out.println("Enter the service code.\n");
			serviceCode = input.nextLine();
			nameOfService = proDirectory.getServiceName(serviceCode);
			if (nameOfService.equals("Invalid")) { //If Service does not exist
				System.out.println("Error: Code does not exist\n");
			}
			else { //If Service exists.
				System.out.println(nameOfService + "\nIs this the service that was provided? (Enter yes or no.)\n");
				answer = input.nextLine();
				if (answer.equals("Yes") || answer.equals("yes")) {
					match = false;
					System.out.println("Optional: Enter any comments. (100 Characters or less)\n");
					comments = input.nextLine();
					while (comments.length() > 100){
						System.out.print("Error: Comment length should be 100 characters or less\n");
						System.out.println("Optional: Enter any comments. (100 Characters or less)\n");
						comments = input.nextLine();
					}
					System.out.println("A copy of this report is being saved.\n");
					now = LocalDateTime.now();
					currentTime = dtf.format(now);
					servRecord.addServiceRecord(month, day, year, currentTime, dateOfService, memberNumber, providerNumber, serviceCode, comments, proDirectory.getServiceFee(serviceCode),proRecord.getProviderName(providerNumber), memRecord.getMemberName(memberNumber));
					createEFTData(); // CHECK TO MAKE SURE THIS IS IN RIGHT SPOT
					System.out.println("Fee to be paid: " + proDirectory.getServiceFee(serviceCode)+ "\n");
					System.out.println("Verification Process\n");
					
					System.out.println("\nEnter current month. (Enter in digit form. Example: March is 03.)\n");
					month = input.nextLine();
					while (month.length() != 2) {
						System.out.println("Error: Month input should be two characters.\n");
						System.out.println("Enter current month. (Enter in digit form. Example: March is 03.)\n");
						month = input.nextLine();
					}
					System.out.println("Enter current day. (Two digit form. Example: The third day of a month is 03.)\n");
					day = input.nextLine();
					while (day.length() != 2) {
						System.out.println("Error: Day input should be two characters.\n");
						System.out.println("Enter current day. (Two digit form. Example: The third day of a month is 03.)\n");
						day = input.nextLine();
					}
					System.out.println("Enter current year. (Four digit form)\n");
					year = input.nextLine();
					while (year.length() != 4) {
						System.out.println("Error: Year input should be four characters.\n");
						System.out.println("Enter current year. (Four digit form)\n");
						year = input.nextLine();
					}
					dtf = DateTimeFormatter.ofPattern("MM-dd-yyyy");
					currentDate = month + "-" + day + "-" + year;
					now = LocalDateTime.now();
					currentTime = dtf.format(now);
					System.out.println("\n" + currentTime + "\n");
					match = currentDate.equals(currentTime);
					while (match == false) {
						System.out.println("Error: Current date input does not match.");
						System.out.println("\nEnter current month. (Enter in digit form. Example: March is 03.)\n");
						month = input.nextLine();
						while (month.length() != 2) {
							System.out.println("Error: Month input should be two characters.\n");
							System.out.println("Enter current month. (Enter in digit form. Example: March is 03.)\n");
							month = input.nextLine();
						}
						System.out.println("Enter current day. (Two digit form. Example: The third day of a month is 03.)\n");
						day = input.nextLine();
						while (day.length() != 2) {
							System.out.println("Error: Day input should be two characters.\n");
							System.out.println("Enter current day. (Two digit form. Example: The third day of a month is 03.)\n");
							day = input.nextLine();
						}
						System.out.println("Enter current year. (Four digit form)\n");
						year = input.nextLine();
						while (year.length() != 4) {
							System.out.println("Error: Year input should be four characters.\n");
							System.out.println("Enter current year. (Four digit form)\n");
							year = input.nextLine();
						}
						dtf = DateTimeFormatter.ofPattern("MM-dd-yyyy");
						currentDate = month + "-" + day + "-" + year;
						now = LocalDateTime.now();
						currentTime = dtf.format(now);
						match = currentDate.equals(currentTime);
						
					}
					//Check with current date
					System.out.println("Enter the month the service was given. (Enter in digit form. Example: March is 03.)\n");
					month = input.nextLine();
					while (month.length() != 2) {
						System.out.println("Error: Month input should be two characters.\n");
						System.out.println("Enter the month the service was given. (Enter in digit form. Example: March is 03.)\n");
						month = input.nextLine();
					}
					System.out.println("Enter the day the service was given.(Two digit form. Example: The third day of a month is 03.)\n");
					day = input.nextLine();
					while (day.length() != 2) {
						System.out.println("Error: Day input should be two characters.\n");
						System.out.println("Enter the day the service was given.(Two digit form. Example: The third day of a month is 03.)\n");
						day = input.nextLine();
					}
					System.out.println("Enter the year the service was given.(Four digit form)\n");
					year = input.nextLine();
					while (year.length() != 4) {
						System.out.println("Error: Year input should be four characters.\n");
						System.out.println("Enter the year the service was given.(Four digit form)\n");
						year = input.nextLine();
					}
					checkServiceDate = month + "-" + day + "-" + year;
					match = checkServiceDate.equals(dateOfService);
					while (match == false) {
						System.out.println("Service date does not match.\n");
						System.out.println("Enter the month the service was given. (Enter in digit form. Example: March is 03)\n");
						month = input.nextLine();
						while (month.length() != 2) {
							System.out.println("Error: Month input should be two characters.\n");
							System.out.println("Enter the month the service was given. (Enter in digit form. Example: March is 03.)\n");
							month = input.nextLine();
						}
						System.out.println("Enter the day the service was given.(Two digit form. Example: The third day of a month is 03\n");
						day = input.nextLine();
						while (day.length() != 2) {
							System.out.println("Error: Day input should be two characters.\n");
							System.out.println("Enter the day the service was given.(Two digit form. Example: The third day of a month is 03.)\n");
							day = input.nextLine();
						}
						System.out.println("Enter the year the service was given.(Four digit form)\n");
						year = input.nextLine();
						while (year.length() != 4) {
							System.out.println("Error: Year input should be four characters.\n");
							System.out.println("Enter the year the service was given.(Four digit form)\n");
							year = input.nextLine();
						}
						checkServiceDate = month + "-" + day + "-" + year;
						match = checkServiceDate.equals(dateOfService);
					}
					//compare checkServiceDate and dateOfService
					System.out.println("Enter the member number for verification.\n");
					checkMemberNumber = input.nextLine();
					match = checkMemberNumber.equals(memberNumber);
					while (match == false) {
						System.out.println("Member number does not match.\n");
						System.out.println("Enter the member number for verification.\n");
						checkMemberNumber = input.nextLine();
						match = checkMemberNumber.equals(memberNumber);
					}
					System.out.println("Enter the member name for verification.\n");
					memberName = input.nextLine();
					match = memberName.equals(memRecord.getMemberName(memberNumber));
					while (match == false) {
						System.out.println("Member name does not match.\n");
						System.out.println("Enter the member name for verification.\n");
						memberName = input.nextLine();
						match = memberName.equals(memRecord.getMemberName(memberNumber));
					}
					//compare member name and number
					System.out.println("Enter the service code for verification.\n");
					checkServiceCode = input.nextLine();
					match = checkServiceCode.equals(serviceCode);
					while (match == false) {
						System.out.println("Service code does not match.\n");
						System.out.println("Enter the service code for verification.\n");
						checkServiceCode = input.nextLine();
						match = checkServiceCode.equals(serviceCode);
					}
					//compare service codes
					System.out.println("Enter the service fee for verification.\n");
					checkServiceFee = input.nextLine();
					match = checkServiceFee.equals(String.valueOf(proDirectory.getServiceFee(serviceCode)));
					while (match == false) {
						System.out.println("Service fee does not match.\n");
						System.out.println("Enter the service fee for verification.\n");
						checkServiceFee = input.nextLine();
						match = checkServiceFee.equals(String.valueOf(proDirectory.getServiceFee(serviceCode)));
					}
					//compare service fees.
					System.out.println("Successful verification. Record written to disk");
					
					
				}
				else { //If answer = no
					System.out.println("Returning to provider menu"); 
				}
			}
		}
		else { //match = false
			System.out.println("Invalid member number");
		}
	}
	
	/**
	 * Method {@code requestProviderDirectory() requests the list of services. The provider directory is also displayed on the terminal
	 * @return Nothing.
	 */
	public static void requestProviderDirectory() throws IOException {
		proDirectory.display();
		System.out.println("The provider directory has been emailed.\n");
		return;
	}
	
	/**
	 * Method {@code manageAccount(String,String)} allows management of accounts.
	 * @param action		this is the action to be taken
	 * @param accountType	{@code Provider}, {@code Member}, or {@code Operator}
	 * @return Nothing.
	 * @throws IOException
	 */
	public static void manageAccount(String action, String accountType) throws IOException {
		boolean match = false;
		String number = "";
		String name = "";
		String streetAddress = "";
		String city = "";
		String state = "";
		String zip = "";
		String address = "";
		if (action == "Create") {
			System.out.println("Enter the account name. (25 Characters or less)\n");
			name = input.nextLine();
			while(name.length() > 25) {
				System.out.println("Error: Name input should be 25 characters or less.");
				System.out.println("Enter the account name. (25 Characters or less)\n");
				name = input.nextLine();
			}
			System.out.println("Enter the account street address. (25 Characters or less)\n");
			streetAddress = input.nextLine();
			while (streetAddress.length() > 25) {
				System.out.println("Error: Street address input should be 25 characters or less.");
				System.out.println("Enter the account street address. (25 Characters or less)\n");
				streetAddress = input.nextLine();
			}
			System.out.println("Enter the account city. (14 Characters or less)\n");
			city = input.nextLine();
			while (city.length() > 14) {
				System.out.println("Error: City input should be 14 characters or less.\n");
				System.out.println("Enter the account city. (14 Characters or less)\n");
				city = input.nextLine();
			}
			System.out.println("Enter the account state. (Use state abbreviation)\n");
			state = input.nextLine();
			while (state.length() != 2) {
				System.out.println("Error: State input should be 2 characters.\n");
				System.out.println("Enter the account state. (Use state abbreviation)\n");
				state = input.nextLine();
			}
			System.out.println("Enter the account zip code.\n");
			zip = input.nextLine();
			while (zip.length() != 5) {
				System.out.println("Error: Zip code input should be 5 characters.\n");
				System.out.println("Enter the account zip code.\n");
				zip = input.nextLine();
			}
		
			address = streetAddress + ", " + city + ", " + state + " " + zip;
			if (accountType == "Provider")
			proRecord.createProvider(name, address);
			if (accountType == "Member")
			memRecord.createMember(name, address);
			if (accountType == "Operator")
			opeRecord.createOperator(name, address);
		}
		if (action == "Update") {
			System.out.println("Enter the account number.\n");
			number = input.nextLine();
			if (accountType == "Provider")
				match = proRecord.verifyProvider(number);
			if (accountType == "Member")
				match = memRecord.verifyMember(number);
			if (accountType == "Operator")
				match = opeRecord.verifyOperator(number);
			if (match == true) {
				System.out.println("If changes are not needed to a certain field, enter the same information\n");
				System.out.println("Enter the account name. (25 Characters or less)\n");
				name = input.nextLine();
				while(name.length() > 25) {
					System.out.println("Error: Name input should be 25 characters or less.");
					System.out.println("Enter the account name. (25 Characters or less)\n");
					name = input.nextLine();
				}
				System.out.println("Enter the account street address. (25 Characters or less)\n");
				streetAddress = input.nextLine();
				while (streetAddress.length() > 25) {
					System.out.println("Error: Street address input should be 25 characters or less.");
					System.out.println("Enter the account street address. (25 Characters or less)\n");
					streetAddress = input.nextLine();
				}
				System.out.println("Enter the account city. (14 Characters or less)\n");
				city = input.nextLine();
				while (city.length() > 14) {
					System.out.println("Error: City input should be 14 characters or less.\n");
					System.out.println("Enter the account city. (14 Characters or less)\n");
					city = input.nextLine();
				}
				System.out.println("Enter the account state. (Use state abbreviation)\n");
				state = input.nextLine();
				while (state.length() != 2) {
					System.out.println("Error: State input should be 2 characters.\n");
					System.out.println("Enter the account state. (Use state abbreviation)\n");
					state = input.nextLine();
				}
				System.out.println("Enter the account zip code.\n");
				zip = input.nextLine();
				while (zip.length() != 5) {
					System.out.println("Error: Zip code input should be 5 characters.\n");
					System.out.println("Enter the account zip code.\n");
					zip = input.nextLine();
				}
	
				address = streetAddress + ", " + city + ", " + state + " " + zip;
				if (accountType == "Provider") {
					proRecord.updateProvider(number, name, address);
				}
				if (accountType == "Member") {
					memRecord.updateMember(number, name, address);
				}
				if (accountType == "Operator") {
					opeRecord.updateOperator(number, name, address);
				}
			}
			else {
				System.out.println("Invalid input\n");
			}
		}
		if (action == "Delete") {
			System.out.println("Enter the account number.\n");
			number = input.nextLine();
			if (accountType == "Provider")
				match = proRecord.verifyProvider(number);
			if (accountType == "Member")
				match = memRecord.verifyMember(number);
			if (accountType == "Operator")
				match = opeRecord.verifyOperator(number);
			if (match == true) {
				if (accountType == "Provider") {
					proRecord.deleteProvider(number);
				}
				if (accountType == "Member") {
					memRecord.deleteMember(number);
				}
				if (accountType == "Operator") {
					opeRecord.deleteOperator(number);
				}
			}
			else {
				System.out.println("Invalid input\n");
			}
		}
		
		return;
	}
	
	/**
	 * Method {@code beginMemberReports()} starts the creation of member reports.
	 * @return Nothing.
	 * @throws IOException
	 */
	public static void beginMemberReports() throws IOException{
		int i;
		int j;
		int count = 1;
		String currentDate;
		System.out.println("\nMaking member reports...");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM-dd-yyyy");
		LocalDateTime now = LocalDateTime.now();
		currentDate = dtf.format(now);
		currentDate.replace("-", "");
		String memberAddress;
		String memberName;
		ArrayList<String> uniqueList = new ArrayList<String>();
		for (i = 0; i < servRecord.getSize(); i++) {
			for (j = 0; j < uniqueList.size(); j++) {
				if (servRecord.getMemberNumber(i).equals(uniqueList.get(j))) {
					break;
				}
			}
			if (j == uniqueList.size()) {
				uniqueList.add(servRecord.getMemberNumber(i));
			}
		}
		for (i = 0; i < uniqueList.size(); i++) {
			memberAddress = memRecord.getMemberAddress(uniqueList.get(i));
			memberName = memRecord.getMemberName(uniqueList.get(i));
			ArrayList<String> serviceInfo = new ArrayList<String>();
			for (j = 0; j < servRecord.getSize(); j++) {
				if (uniqueList.get(i).equals(servRecord.getMemberNumber(j))) {
					serviceInfo.add(count + ". " + servRecord.getDateOfService(j));
					serviceInfo.add(servRecord.getProviderName(j));
					serviceInfo.add(proDirectory.getServiceName(servRecord.getServiceCode(j)));
					serviceInfo.add("\n");
					count = count + 1;
				}
			
			}
			count = 1;
			memReport.createMemberReport(memberName, uniqueList.get(i), memberAddress, serviceInfo, currentDate);
		}
		return;
	}
	
	/**
	 * Method {@code beginProviderReports()} starts the creation of provider reports.
	 * @return Nothing.
	 * @throws IOException
	 */
	public static void beginProviderReports() throws IOException{
		int i;
		int j;
		int count = 1;
		int consult = 0;
		String currentDate;
		System.out.println("\nMaking provider reports...");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM-dd-yyyy");
		LocalDateTime now = LocalDateTime.now();
		currentDate = dtf.format(now);
		currentDate.replace("-", "");
		String providerAddress;
		String providerName;
		ArrayList<String> uniqueList = new ArrayList<String>();
		for (i = 0; i < servRecord.getSize(); i++) {
			for (j = 0; j < uniqueList.size(); j++) {
				if (servRecord.getProviderNumber(i).equals(uniqueList.get(j))) {
					break;
				}
			}
			if (j == uniqueList.size()) {
				uniqueList.add(servRecord.getProviderNumber(i));
			}
		}
		for (i = 0; i < uniqueList.size(); i++) {
			providerAddress = proRecord.getProviderAddress(uniqueList.get(i));
			providerName = proRecord.getProviderName(uniqueList.get(i));
			ArrayList<String> serviceInfo = new ArrayList<String>();
			for (j = 0; j < servRecord.getSize(); j++) {
				if (uniqueList.get(i).equals(servRecord.getProviderNumber(j))) {
					serviceInfo.add(count + ". " + servRecord.getDateOfService(j));
					serviceInfo.add(servRecord.getCurrentTime(j));
					serviceInfo.add(servRecord.getMemberName(j));
					serviceInfo.add(servRecord.getMemberNumber(j));
					serviceInfo.add(proDirectory.getServiceName(servRecord.getServiceCode(j)));
					serviceInfo.add(String.valueOf(proDirectory.getServiceFee(servRecord.getServiceCode(j))));
					serviceInfo.add("\n");
					count = count + 1;
				}
				
			}
			consult = count - 1;
			proReport.createProviderReport(providerName, uniqueList.get(i), providerAddress, serviceInfo, currentDate, consult, servRecord.getTotalFee(uniqueList.get(i)));
			count = 1;
		}
		return;
	}
	
	/**
	 * Method {@code beginSummaryReport()} starts the creation of a summary report.
	 * @return Nothing.
	 * @throws IOException
	 */
	public static void beginSummaryReport() throws IOException {	//Changed from Report to void
		int i;
		int j;
		int consult;
		double totalFee;
		ArrayList<String> uniqueList = new ArrayList<String>(); // NEW
		String providerNumber;
		for (i = 0; i < servRecord.getSize(); i++) {
			for (j = 0; j < uniqueList.size(); j++) {
				if (servRecord.getProviderNumber(i).equals(uniqueList.get(j))) {
					break;
				}
			}
			if (j == uniqueList.size()) {
				uniqueList.add(servRecord.getProviderNumber(i));
			}
		}
		for (i = 0; i < uniqueList.size(); i++) {
			providerNumber = uniqueList.get(i);
			consult = servRecord.getCount(providerNumber);
			totalFee = servRecord.getTotalFee(providerNumber);
			sumReport.addSummaryReport(proRecord.getProviderName(providerNumber), providerNumber, consult, totalFee);
		}
		sumReport.emptyLists(uniqueList.size());
		return;
	}
	
	/**
	 * Method {@code createEFTData()} creates data for the EFT system.
	 * @return Nothing.
	 * @throws IOException
	 */
	public static void createEFTData() throws IOException {
		int i;
		int j;
		String providerNumber;
		String providerName;
		double totalFee;
		ArrayList<String> uniqueList = new ArrayList<String>();
		for (i = 0; i < servRecord.getSize(); i++) {
			for (j = 0; j < uniqueList.size(); j++) {
				if (servRecord.getProviderNumber(i).equals(uniqueList.get(j))) {
					break;
				}
			}
			if (j == uniqueList.size()) {
				uniqueList.add(servRecord.getProviderNumber(i));
			}
		}
		for (i = 0; i < uniqueList.size(); i++) {
			providerNumber = uniqueList.get(i);
			providerName = proRecord.getProviderName(providerNumber);
			totalFee = servRecord.getTotalFee(providerNumber);
			EFTRecord.createEFTRecord(providerNumber, providerName, totalFee);	
		}
		beginSummaryReport();
		return;
	}

}