package project4;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.ArrayList;

/**
 * Class {@code OperatorRecord} creates a file containing all existing operators.
 * @author James Tubbs
 */
public class OperatorRecord {
	private ArrayList<Operator> operatorList = new ArrayList<Operator>();
	private Path operatorFile;
	private int nextNumberInt; //Used for operator numbers
	private String nextNumber; //Used to convert operator number to string
	private int size;
	
	/**
	 * Constructor {@code OperatorRecord()} initializes variables to be used for operator information.
	 */
	public OperatorRecord() {
		size = 0;
		nextNumberInt = 100000000;
		nextNumber = String.valueOf(nextNumberInt);
	}
	
	/**
	 * Getter {@code getOperatorName(String)} retrieves the operator's name.
	 * @param operatorNumber uses this to find the right operator
	 * @return the name of this operator
	 */
	public String getOperatorName(String operatorNumber) {
		int i;
		for (i = 0; i < size; i++) {
			if (operatorNumber.equals(operatorList.get(i).getOperatorNumber())) {
				break;
			}
		}
		return operatorList.get(i).getOperatorName();
	}
	
	/**
	 * Method {@code deleteFile()} deletes the file.
	 * @return Nothing.
	 * @throws IOException
	 */
	public void deleteFile() throws IOException {
		Path files;
		files = Paths.get("operator_record.txt");
		Files.delete(files);
		
		return;
	}
	
	/**
	 * Method {@code verifyOperator} represents a sort of login functionality for operators.
	 * @param operatorNumber this is the password for the login
	 * @return true or false based on verification
	 */
	public boolean verifyOperator(String operatorNumber) { 
		int j = -1;
		int i;
		for (i = 0; i < size; i++) {
			if (operatorNumber.equals(operatorList.get(i).getOperatorNumber())) {
				j = i;
				break;
			}
		}
		
		if (j >= 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Method {@code createOperator(String,String)} creates a new operator.
	 * @param name    this is the name for the new operator
	 * @param address this is the address for the new operator
	 * @return  Nothing.
	 * @throws IOException 
	 */
	public void createOperator(String name, String address) throws IOException{
		Operator newOperator = new Operator(nextNumber,name,address);
		operatorList.add(newOperator);
	
		System.out.println("An operator account for " + name + " has been created.\nThe operator number for this account is " + nextNumber + ".\n");
		size = size + 1;
		nextNumberInt = nextNumberInt + 1;
		nextNumber = String.valueOf(nextNumberInt);
		
		int i;
		operatorFile = Paths.get("operator_record.txt"); //MAKES THE FILE
		ArrayList<String> lines = new ArrayList<String>();
		for (i=0; i < size; i++) {
			lines.add(String.valueOf(i+1)+ ". " + operatorList.get(i).getOperatorName() + " " + operatorList.get(i).getOperatorNumber() + " " + operatorList.get(i).getOperatorAddress() + "\n");
	
		}
		Files.write(operatorFile, lines);
		
		return;
	}
	
	/**
	 * Method {@code updateOperator(String,String,String)} sets new values for operator attributes.
	 * @param number     new password
	 * @param newName    new name
	 * @param newAddress new address
	 * @return Nothing.
	 */
	public void updateOperator(String number, String newName, String newAddress)throws IOException {
		int i = -1;
		
		for (i = 0; i < size; i++) {
			if (number.equals(operatorList.get(i).getOperatorNumber()))
			break;
		}
		operatorList.get(i).setOperatorName(newName);
		operatorList.get(i).setOperatorAddress(newAddress);
		System.out.println("Operator has been updated.\n");
		
		operatorFile = Paths.get("operator_record.txt"); //MAKES THE FILE
		ArrayList<String> lines = new ArrayList<String>();
		for (i=0; i < size; i++) {
			lines.add(String.valueOf(i+1)+ ". " + operatorList.get(i).getOperatorName() + " " + operatorList.get(i).getOperatorNumber() + " " + operatorList.get(i).getOperatorAddress() + "\n");
	
		}
		Files.write(operatorFile, lines);
		
		return;
	}
	
	/**
	 * Method {@code deleteOperator(String)} removes an operator.
	 * @param number password
	 * @return Nothing.
	 */
	public void deleteOperator(String number)throws IOException {
		int i = -1;
		
		for (i = 0; i < size; i++) {
			if (number.equals(operatorList.get(i).getOperatorNumber()))
			break;
		}
		operatorList.remove(i);
		System.out.println("Operator has been deleted.\n");
		size = size - 1;

		operatorFile = Paths.get("operator_record.txt"); //MAKES THE FILE
		ArrayList<String> lines = new ArrayList<String>();
		for (i=0; i < size; i++) {
			lines.add(String.valueOf(i+1)+ ". " + operatorList.get(i).getOperatorName() + " " + operatorList.get(i).getOperatorNumber() + " " + operatorList.get(i).getOperatorAddress() + "\n");
	
		}
		Files.write(operatorFile, lines);
		
		return;
	}
}