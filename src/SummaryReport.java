package project4;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;


/**
 * Class {@code SummaryReport} inherits from {@code Report} and facilitates a summary report.
 * @author Ze Miao
 */
public class SummaryReport {
	private ArrayList<String> providerNameList;
	private ArrayList<String> providerNumberList;
	private ArrayList<Integer> consultList;
	private ArrayList<Double> totalFeeList;
	private Path summaryFile;
	
	/**
	 * Constructor {@code SummaryReport} creates a new summary report.
	 */
	public SummaryReport() {
		providerNameList = new ArrayList<String>();
		providerNumberList = new ArrayList<String>();
		consultList = new ArrayList<Integer>();
		totalFeeList = new ArrayList<Double>();
	}
	
	/**
	 * Method {@code addSummaryReport(String,String,int,double} creates a new summary report.
	 * @param providerName		name of the provider
	 * @param providerNumber	the provider's password
	 * @param consult			the consulting fee
	 * @param totalFee 			the total fee
	 * @return Nothing.
	 * @throws IOException 
	 */
	public void addSummaryReport(String providerName, String providerNumber, int consult, double totalFee) throws IOException {	//Changed from summary report to void
		double overallFee = 0.0;
		int totalConsult = 0;
		int i;
		ArrayList<String> lines = new ArrayList<String>();
		providerNameList.add(providerName);
		providerNumberList.add(providerNumber);
		consultList.add(consult);
		totalFeeList.add(totalFee);
		
		for (i = 0; i < consultList.size(); i++) {
			lines.add(String.valueOf(i+1) + ". " + providerNameList.get(i));
			lines.add("Provider Number: " + providerNumberList.get(i));
			lines.add("Number of Consultations: " + String.valueOf(consultList.get(i)));
			lines.add("Total Fee: $" + String.valueOf(totalFeeList.get(i)));
			lines.add("\n");
			totalConsult = totalConsult + consultList.get(i);
			overallFee = overallFee + totalFeeList.get(i);
		}
		
		lines.add("Total Number of Providers: " + String.valueOf(providerNameList.size()));
		lines.add("Total Number of Consultations: " + String.valueOf(totalConsult));
		lines.add("Overall Fee Total: $" + String.valueOf(overallFee));
		summaryFile = Paths.get("summary_report.txt");
		Files.write(summaryFile, lines);	
		return;
	}
	
	/**
	 * Method {@code deleteFile()} deletes the files.
	 * @return Nothing.
	 * @throws IOException
	 */
	public void deleteFile() throws IOException {
		Path files;
		files = Paths.get("summary_report.txt");
		Files.delete(files);
		
		return;
	}
	
	/**
	 * Method {@code emptyLists()} empties list of provider names and numbers, as well as the list of consultations and total fees.
	 * @param size size of the lists
	 * @return Nothing
	 */
	public void emptyLists(int size){
		int i;
		for (i = size - 1; i>= 0; i--){
			providerNameList.remove(i);
			providerNumberList.remove(i);
			consultList.remove(i);
			totalFeeList.remove(i);
		}
		return;
	}
}