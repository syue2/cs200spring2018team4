package project4; 

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import project4.MemberRecord;

public class MemberRecordTest {

	@Test // Test Success
	public void testGetMemberName() throws IOException {
		MemberRecord memRecord = new MemberRecord();
		memRecord.createMember("mike","123 Alabama Blvd");
		assertEquals("mike",memRecord.getMemberName("100000000"));
	}

	@Test // Test Failure
	public void testUpdateMember() throws IOException {
		MemberRecord memRecord = new MemberRecord();
		memRecord.createMember("mike","123 Alabama Blvd");
		memRecord.updateMember("100000000","james","1234 Alabama Blvd");
		assertNotEquals("mike", memRecord.getMemberName("100000000"));
	}

	@Test // Test Success
	public void testGetServiceCode() throws IOException {
		MemberRecord memRecord = new MemberRecord();
		memRecord.createMember("mike","123 Alabama Blvd");
		memRecord.deleteMember("100000000");
		assertEquals(false,memRecord.verifyMember("100000000"));
	}
}