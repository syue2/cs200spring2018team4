package project4;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Class {@code ServiceRecord} facilitates the records of a service.
 * @author Blake Pennington
 */
public class ServiceRecord {
	
	private ArrayList<String> providerNumberList = new ArrayList<String>();
	private ArrayList<String> providerNameList = new ArrayList<String>();
	private ArrayList<String> memberNumberList = new ArrayList<String>();
	private ArrayList<String> memberNameList = new ArrayList<String>();
	private ArrayList<String> currentTime = new ArrayList<String>();
	private ArrayList<String> dateOfServiceList = new ArrayList<String>();
	private List<Double> feeList = new ArrayList<>(); 
	private ArrayList<String> serviceInfoList = new ArrayList<String>();
	private ArrayList<String> serviceCodeList = new ArrayList<String>();
	private ArrayList<String> comments = new ArrayList<String>();
	private Path serviceFile;
	private int size;
	
	/**
	 * Constructor {@code ServiceRecord()} creates a new service record.
	 */
	public ServiceRecord() {
		size = 0;
		
	}
	
	/**
	 * Getter {@code getTotalFee(String)} retrieves the total fee given the provider number.
	 * @param providerNumber the password of the provider
	 * @return the total fee
	 */
	public double getTotalFee(String providerNumber) {
		double totalFee = 0;
		int i;
		for (i=0; i<providerNumberList.size(); i++) {
			if (providerNumber.equals(providerNumberList.get(i))) {
				totalFee = feeList.get(i) + totalFee;
			}
		}
		
		return totalFee;
	}
	
	/**
	 * Getter {@code getCount(String)} retrieves the count given the provider number.
	 * @param providerNumber the password of the provider
	 * @return count the count
	 */
	public int getCount(String providerNumber) {
		int i;
		int count = 0;
		for (i = 0; i < providerNumberList.size(); i++) {
			if (providerNumberList.get(i).equals(providerNumber)) {
				count = count + 1;
			}
		}
		
		return count;
	}
	
	/**
	 * Method {@code deleteFile()} deletes the file.
	 * @return Nothing.
	 * @throws IOException
	 */
	public void deleteFile() throws IOException {
		Path files;
		files = Paths.get("service_record.txt");
		Files.delete(files);
		
		return;
	}
	
	/**
	 * Getter {@code getSize()} retrieves the size of the list of providers.
	 * @return the size of the list of providers
	 */
	public int getSize() {
		return size;
	}
	
	/** 
	 * Getter {@code getProviderName(int)} retrieves the name of the provider name given its spot in the list.
	 * @param index spot in the list
	 * @return the name of the provider
	 */
	public String getProviderName(int index) {
		if (index > size)
			return "Error: The index value is too large\n";
		return providerNameList.get(index);
	}
	
	/**
	 * Getter {@code getProviderNumber(int)} retrieves the provider number given its spot in the list.
	 * @param index spot in the list
	 * @return the password of the provider
	 */
	public String getProviderNumber(int index) {
		return providerNumberList.get(index);
	}
	
	/**
	 * Getter {@code getDateOfService(int)} retrieves the date of service given its spot in the list.
	 * @param index spot in the list
	 * @return the date of service
	 */
	public String getDateOfService (int index) {
		return dateOfServiceList.get(index);
	}
	
	/**
	 * Getter {@code getMemberName(int)} retrieves the member's name given its spot in the list.
	 * @param index spot in the list
	 * @return the member's name
	 */
	public String getMemberName(int index) {
		return memberNameList.get(index);
	}
	
	/**
	 * Getter {@code getMemberNumber(int)} retrieves the member's password given its spot in the list.
	 * @param index spot in the list
	 * @return the member's password
	 */
	public String getMemberNumber(int index) {
		return memberNumberList.get(index);
	}
	
	/**
	 * Getter {@code getCurrentTime(int)} retrieves the current time given its spot in the list.
	 * @param index spot in the list
	 * @return the current time
	 */
	public String getCurrentTime(int index) {
		return currentTime.get(index);
	}
	
	/**
	 * Getter {@code getServiceCode(int)} retrieves the service code given its spot in the list.
	 * @param index spot in the list
	 * @return the service code
	 */
	public String getServiceCode(int index) {
		return serviceCodeList.get(index);
	}
	
	/**
	 * Method {@code addServiceRecord(String,String,String,String,String,String,String,String,String,double,String,String) adds a new record of a service
	 * @param month				the month
	 * @param day				the day
	 * @param year				the year
	 * @param currentTime 		the current time
	 * @param dateOfService 	the date of service
	 * @param memberNumber 		the password of the member
	 * @param providerNumber 	the provider password
	 * @param serviceCode 		the service code
	 * @param comments 			comments concerning the service
	 * @param serviceFee 		the cost of the service
	 * @param providerName 		the name of the provider
	 * @param memberName		the name of the member
	 * @throws IOException
	 * @return Nothing.
	 */
	public void addServiceRecord(String month, String day, String year, String currentTime, String dateOfService, String memberNumber, String providerNumber, String serviceCode, String comments, double serviceFee, String providerName, String memberName) throws IOException {
		serviceCodeList.add(serviceCode);
		dateOfServiceList.add(dateOfService);
		providerNumberList.add(providerNumber);
		providerNameList.add(providerName);
		memberNumberList.add(memberNumber);
		memberNameList.add(memberName);
		this.comments.add(comments);
		this.currentTime.add(currentTime);
		feeList.add(serviceFee);
		serviceInfoList.add(String.valueOf(size +1)+ ". " + "Date of Service: " + dateOfService);
		serviceInfoList.add("Data Entry Time: " + currentTime);
		serviceInfoList.add("Provider Number: " + providerNumber);
		serviceInfoList.add("Member Number: " + memberNumber);
		serviceInfoList.add("Service Code: " + serviceCode);
		serviceInfoList.add("Comments: " + comments);
		serviceInfoList.add("\n");
		size = size + 1;
		serviceFile = Paths.get("service_record.txt"); //MAKES THE FILE
		Files.write(serviceFile, serviceInfoList); //WRITES TO FILE
		
		return;
	}
	
}
