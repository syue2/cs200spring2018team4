package project4; 

import static org.junit.Assert.*;
import project4.ServiceRecord;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class ServiceRecordTest {

	@Before
	public void setUp() throws Exception {
	}
	
	@Test
	//TESTS THE GETSERVICECODE CORRECTLY RETURNS THE VALUE
	public void testSuccess() throws IOException {
		ServiceRecord servRecord = new ServiceRecord();
		servRecord.addServiceRecord("02", "02", "1998", "4-24-3928", "04-24-1998","100000000", "100000000", "420420", "ok", 34.23, "Blake", "Tanner");
		assertEquals(servRecord.getServiceCode(0), "420420");
	} 
	
	//TESTS THE FUNCTIONALITY OF THE STORAGE OF SERVICE FEES
	@Test
	public void testSuccess2() throws IOException {
		ServiceRecord servRecord = new ServiceRecord();
		servRecord.addServiceRecord("02", "02", "1998", "4-24-3928", "04-24-1998","100000000", "100000000", "420420", "ok", 34.28, "Blake", "Tanner");
		assertEquals(String.valueOf(34.28), String.valueOf(servRecord.getTotalFee("100000000")));
		
	}
	
	//TESTS THAT GET FUNCTION CAN'T ACCESS RANGES THAT DON'T EXIST
	@Test
	public void testFailure() throws IOException {
		ServiceRecord servRecord = new ServiceRecord();
		servRecord.addServiceRecord("02", "02", "1998", "4-24-3928", "04-24-1998","100000000", "100000000", "420420", "ok", 34.23, "Blake", "Tanner");
		assertEquals("Error: The index value is too large\n",servRecord.getProviderName(100) );
	}
	

}
