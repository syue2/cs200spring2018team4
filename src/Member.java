package project4;


/**
 * Class {@code Member} is comprised of attributes of a ChocAn member.
 * @author Ze Miao
 */
public class Member {
	private String number;
	private String name;
	private String address;
	
	/**
	 * Constructor {@code Member(String,String,String)} initializes each attribute upon addition of a new member.
	 * @param number  A member's password
	 * @param name    A member's name
	 * @param address A member's address
	 */
	public Member(String number, String name, String address) {
		this.number = number;
		this.name = name;
		this.address = address;
	}
	
	/**
	 * Getter {@code getMemberName()} retrieves attribute name.
	 * @return attribute name
	 */
	public String getMemberName() {
		return name;
	}
	
	/**
	 * Getter {@code getMemberNumber()} retrieves attribute number.
	 * @return attribute number
	 */
	public String getMemberNumber() {
		return number;
	}
	
	/**
	 * Getter {@code getMemberAddress()} retrieves attribute address.
	 * @return attribute address
	 */
	public String getMemberAddress() {
		return address;
	}
	
	/**
	 * Setter {@code setMemberName(String)} updates attribute name.
	 * @param name this updates the old name
	 * @return Nothing.
	 */
	public void setMemberName(String name) {
		this.name = name;
		return;
	}

	/**
	 * Setter {@code setMemberAddress(String)} updates attribute address.
	 * @param address this updates the old address
	 * @return Nothing.
	 */
	public void setMemberAddress(String address) {
		this.address = address;
		return;
	}
}