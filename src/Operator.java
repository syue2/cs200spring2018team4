package project4;

/**
 * Class {@code Operator} contains attributes of an operator.
 * @author Tanner Causey
 */
public class Operator {
	private String number;
	private String name;
	private String address;
	
	/**
	 * Constructor {@code Operator(String,String,String)} creates a new Operator object.
	 * @param number  this is the password
	 * @param name    this is the name
	 * @param address this is the address
	 */
	public Operator(String number, String name, String address) {
		this.number = number;
		this.name = name;
		this.address = address;
	}
	
	/**
	 * Getter {@code getOperatorName()} retrieves name attribute.
	 * @return name attribute
	 */
	public String getOperatorName() {
		return name;
	}
	
	/**
	 * Getter {@code getOperatorNumber()} retrieves number attribute.
	 * @return number attribute
	 */
	public String getOperatorNumber() {
		return number;
	}
	
	/**
	 * Getter {@code getOperatorAddress()} retrieves address attribute.
	 * @return number attribute
	 */
	public String getOperatorAddress() {
		return address;
	}
	
	/**
	 * Setter {@code setOperatorName(String)} updates name attribute.
	 * @param name: name to set
	 * @return Nothing.
	 */
	public void setOperatorName(String name) {
		this.name = name;
		return;
	}
	
	/**
	 * Setter {@code setOperatorAddress(String)} updates address attribute.
	 * @param address: address to set
	 * @return Nothing.
	 */
	public void setOperatorAddress(String address) {
		this.address = address;
		return;
	}
}