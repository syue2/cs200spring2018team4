package project4;
/**
 * Class {@code Service} holds details on services from providers.
 * @author Tanner Causey
 */
public class Service {
    private String name;
    private String code;
    private double fee;
    
    /**
     * Constructor {@code Service(String,String,double)} creates a new service.
     * @param x to be assigned to name
     * @param y to be assigned to code
     * @param z to be assigned to fee
     */
    public Service(String x, String y, double z) {
        this.name = x;
        this.code = y;
        this.fee = z;
    }
    
    /**
     * Getter {@code getServiceName()} retrieves the name of the service.
     * @return name the name of the service
     */
    public String getServiceName() {
        return name;
    }
    
    /**
     * Getter {@code getServiceCode()} retrieves the service code.
     * @return code the service code
     */
    public String getServiceCode() {
        return code;
    }
    
    /**
     * Getter {@code getServiceCode()} retrieves the service fee.
     * @return fee cost of the service.
     */
    public double getServiceFee() {
        return fee;
    }
   
}