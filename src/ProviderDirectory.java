package project4;
import java.util.ArrayList;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;

/**
 * Class {@code ProviderDirectory} contains a list of services provided by ChocAn, along with their services codes and fees.
 * @author Ze Miao
 */
public class ProviderDirectory {
	private int size;
	private ArrayList<Service> serviceList = new ArrayList<Service>();
	
	/**
	 * Constructor {@code ProviderDirectory()} introduces a few sample services, along with their codes and fees.
	 */
	public ProviderDirectory(){
		serviceList.add(new Service("Aerobics Excercise Session", "883948", 45.38));
		serviceList.add(new Service("Choco Rorshach Test","314314", 52.70));
		serviceList.add(new Service("Chocolate Submersion", "365365", 72.34));
		serviceList.add(new Service("Dietician Session", "598470", 134.99));
		serviceList.add(new Service("Electro Shock Therapy", "420420", 34.28));	
		size = 5;
		
	}
	
	/**
	 * Getter {@code getServiceCode(String)} retrieves service code given its name.
	 * @param name name of service
	 * @return the service code
	 */
	public String getServiceCode(String name) {
		int i;
		for (i = 0; i < size; i++) {
			if (name.equals(serviceList.get(i).getServiceName())) 
				break;
		}
		
		if (i == size) {
			return "Invalid";
		}
		return serviceList.get(i).getServiceCode();
	}
	
	/**
	 * Getter {@code getServiceName(String)} retrieves service name given its code.
	 * @author Tanner Causey
	 * @param code service code
	 * @return name of service
	 */
	public String getServiceName(String code) {
		int i;
		for (i = 0; i < size; i++) {
			if (code.equals(serviceList.get(i).getServiceCode()))
				break;
		}
		
		if (i == size) {
			return "Invalid";
		}	
		return serviceList.get(i).getServiceName();
	}
	
	/**
	 * Getter {@code getServiceFee(String)} retrieves service fee given its code.
	 * @param code service code
	 * @return the cost of the service
	 */
	public double getServiceFee(String code) {
		int i;
		for (i = 0; i < size; i++) {
			if (code.equals(serviceList.get(i).getServiceCode()))
				break;
		}
		
		if (i == size) {
			return -1;
		}	
		return serviceList.get(i).getServiceFee();
	}

	/**
	 * {@code display()} shows all services on the terminal, along with their corresponding codes and fees
	 * A text file is made containing the provider directory.
	 * @throws IOException
	 */
	public void display() throws IOException {
		int i;
		System.out.println("\nProvider Directory\n");
		for (i = 0; i < size; i++) {
			System.out.println(i + 1 + ". " + serviceList.get(i).getServiceName() + "\n");
			System.out.println(serviceList.get(i).getServiceCode() + "\n");
			System.out.println(serviceList.get(i).getServiceFee() + "\n");
		}
		Path directoryFile = Paths.get("provider_directory.txt");
		ArrayList<String> lines = new ArrayList<String>();
		for (i=0; i < size; i++) {
			lines.add(String.valueOf(i + 1) + ". " + serviceList.get(i).getServiceName());
			lines.add(serviceList.get(i).getServiceCode());
			lines.add(String.valueOf(serviceList.get(i).getServiceFee()));
			lines.add("\n");
		}
		Files.write(directoryFile, lines);
	}
}