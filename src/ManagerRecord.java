package project4;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.ArrayList;

/**
 * Class {@code ManagerRecord} creates text files, updates and deletes managers, and verifies managers.
 * @author Blake Pennington
 * 
 */

public class ManagerRecord {
	private ArrayList<Manager> managerList;
	private Path managerFile;
	private int size;
	
	/**
	 * Constructor {@code ManagerRecord()} initializes the array of managers.
	 * @author 
	 */

	public ManagerRecord(){
		managerList = new ArrayList<Manager>();
	}
	
	/**
	 * Method {@code addManager()} adds the managers seen below.
	 * @return Nothing.
	 * @throws IOException
	 */
	public void addManager() throws IOException {
		int i;
		ArrayList<String> data = new ArrayList<String>();
		managerFile = Paths.get("manager_record.txt");
		managerList.add(new Manager("11616778", "Blake Pennington", "UA"));
        managerList.add(new Manager("11600650", "Coleman Cost", "UA"));
        managerList.add(new Manager("11672883", "James Tubbs", "UA"));
        managerList.add(new Manager("11488440", "Michael Oram", "UA"));
        managerList.add(new Manager("11667463", "Ze Miao", "UA"));
        managerList.add(new Manager("11687809", "Tanner Causey", "UA"));
        managerList.add(new Manager("12345678", "Frank Yue", "UA"));
        for (i = 0; i < 7; i++){
        	data.add(managerList.get(i).getManagerName() + "     " + managerList.get(i).getManagerNumber() + "     " + managerList.get(i).getManagerAddress() + "\n");
        }
        Files.write(managerFile,data);
		size = 7;
		return;
	}
	
	/**
	 * Method {@code deleteFile()} deletes the file.
	 * @author Tanner Causey
	 * @return Nothing.
	 * @throws IOException
	 */
	public void deleteFile() throws IOException {
		Path files;
		files = Paths.get("manager_record.txt");
		Files.delete(files);
		
		return;
	}
	
	/**
	 * Getter {@code getManagerName(String)} retrieves the manager name.
	 * @author Tanner Causey
	 * @param managerNumber this number helps find the managerName
	 * @return the manager name
	 */
	public String getManagerName(String managerNumber) {
		int i;
		for (i = 0; i < size; i++) {
			if (managerNumber.equals(managerList.get(i).getManagerNumber())) {
				break;
			}
		}
		
		return managerList.get(i).getManagerName();
	}
	
	/**
	 * Method {@code verifyManager(String)} is essentially a login process.
	 * @author Tanner Causey
	 * @param managerNumber this is the password
	 * @return boolean true or false
	 */
	public boolean verifyManager(String managerNumber) { 
		int j = -5;
		int i;
		for (i = 0; i < size; i++) {
			if (managerNumber.equals(managerList.get(i).getManagerNumber())) {
				j = i;
				break;
			}
		}
		
		if (j >= 0) {
			return true;
		}
		else {
			return false;
		}
	}
}