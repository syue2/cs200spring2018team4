package project4; 

import static org.junit.Assert.*;

import java.io.IOException;

import project4.OperatorRecord;
import org.junit.Before;
import org.junit.Test;

public class OperatorRecordTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testVerifyOperator() throws IOException {
		boolean match = true;
		OperatorRecord opeRecord = new OperatorRecord();
		opeRecord.createOperator("Coleman", "UA Tuscaloosa, AL. 39539");
		assertEquals(match, opeRecord.verifyOperator("100000000"));
	}

	@Test
	public void testGetOperatorName() throws IOException {
		OperatorRecord opeRecord = new OperatorRecord();
		opeRecord.createOperator("Coleman", "UA Bessmer, AL. 39539");
		assertEquals("Coleman", opeRecord.getOperatorName("100000000"));
	}
	
	@Test
	public void testDeleteOperator() throws IOException {
		boolean deletion = false;
		OperatorRecord opeRecord = new OperatorRecord();
		opeRecord.createOperator("Coleman", "UA Bessmer, AL. 39539");
		opeRecord.deleteOperator("100000000");
		assertEquals(deletion, opeRecord.verifyOperator("100000000"));
		
	}
}
