package project4;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Class {@code MemberReport} represents member reports.
 * @author Michael Oram
 */
public class MemberReport {
	private ArrayList<String> memberFiles;
	
	/**
	 * Constructor (@code MemberReport} initializes an array of member files.
	 */
	public MemberReport(){
		memberFiles = new ArrayList<String>();
	} 
	
	/**
	 * Method {@code createMemberReport(String,String,String,ArrayList<String>,String)} writes a file containing services received and charges for each member.
	 * @param memberName	the member's name
	 * @param memberNumber	the member's password
	 * @param memberAddress	the member's address
	 * @param serviceInfo	information about services the member has received
	 * @param currentDate	the current date
	 * @return Nothing.
	 * @throws IOException
	 */
	public void createMemberReport(String memberName, String memberNumber, String memberAddress, ArrayList<String> serviceInfo, String currentDate)throws IOException {
		int i;
		memberFiles.add("member_" + memberName + currentDate + ".txt");
		String fileName = "member_" + memberName + currentDate + ".txt";
		Path memberFile = Paths.get(fileName);
		ArrayList<String> data = new ArrayList<String>();
		data.add(memberName);
		data.add("Member Number: " + memberNumber);
		data.add("Member Address: " + memberAddress);
		data.add("\n");
		data.add("Services Received");
		data.add("\n");
		for (i = 0; i < serviceInfo.size(); i++){
		data.add(serviceInfo.get(i));
		}
		Files.write(memberFile, data);
		return;
	}
	
	/**
	 * Method {@code deleteFiles()} deletes the files.
	 * @return Nothing.
	 * @throws IOException
	 */
	public void deleteFiles() throws IOException {
		Path files;
		int i;
		for (i = 0; i < memberFiles.size();i++) {
			files = Paths.get(memberFiles.get(i));
			Files.delete(files);
		}
		return;
	}
	
}