package project4;

import static org.junit.Assert.*;
import java.io.IOException;
import project4.Provider;
import org.junit.Before;
import org.junit.Test;

public class ProviderTest {

	@Before 
	public void setUp() throws Exception {
	}
 
	@Test
	public void testSuccess1() throws IOException {
		Provider newProvider = new Provider("100000000","Ze Miao", "UA");
		assertEquals("Ze Miao", newProvider.getProviderName());
	}
	
	@Test
	public void testSuccess2() throws IOException {
		Provider newProvider = new Provider("100000001", "Ze Miao", "address, Tuscaloosa, AL 35404");
		assertEquals("address, Tuscaloosa, AL 35404",newProvider.getProviderAddress());
	}
	
	@Test
	public void testFailure() throws IOException {
		Provider newProvider = new Provider("100000000", "Ze Miao", "UA");
		assertNotEquals("Bob", newProvider.getProviderName());
	}
}