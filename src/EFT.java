package project4;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Class {@code EFT} covers documentation for services throughout ChocAn at the end of each week.
 * @author Tanner Causey
 */
public class EFT {

	private ArrayList<String> providerNumber; //List of all providers that have provided service
	private ArrayList<String> providerName;
	private ArrayList<String> totalFee; //Corresponds to provider at same index of providerNumber ArrayList
	private Path EFTfile;
	
	/**
	 * Constructor {@code EFT()} initializes the array lists that will eventually hold data.
	 */
	public EFT() {
		 providerNumber = new ArrayList<String>();
		 providerName = new ArrayList<String>();
		 totalFee = new ArrayList<String>();
	
	}

	/**
	 * Method {@code createEFTRecord} writes the record covering all services until Friday for each week.
	 * @param providerNumber 
	 * @param providerName
	 * @param totalFee has the total fee earned by that particular provider
	 * @throws IOException 
	 */
	public void createEFTRecord(String providerNumber, String providerName, double totalFee) throws IOException {
		int i;
		ArrayList<String> list = new ArrayList<String>();
		for (i = 0; i < this.providerNumber.size(); i++){
			if (providerNumber.equals(this.providerNumber.get(i))) {
				break;
			}
		}
		if (i == this.providerNumber.size()) {
			this.providerNumber.add(providerNumber);
			this.providerName.add(providerName);
			this.totalFee.add(String.valueOf(totalFee));
		}
		else {
			this.totalFee.set(i, String.valueOf(totalFee));
		}
		for (i = 0; i < this.providerName.size(); i++) {
			list.add(i + 1 + ". Provider Number: " + this.providerNumber.get(i));
			list.add("Provider Name: " + this.providerName.get(i));
			list.add("Total Fee: " + this.totalFee.get(i));
			list.add("\n");
		}
		EFTfile = Paths.get("eft_record.txt"); //MAKES THE FILE
		Files.write(EFTfile, list); //WRITES TO FILE
		return;
	}
	
	/**
	 * Method {@code deleteFile()} Deletes the EFT record made. Caused by reset of the system.
	 * @throws IOException
	 */
	public void deleteFile() throws IOException {
		Path files;
		files = Paths.get("eft_record.txt");
		Files.delete(files);
		
		return;
	}
}