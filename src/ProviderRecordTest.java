package project4; 

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import project4.ProviderRecord;

public class ProviderRecordTest {

	@Test // Test Success
	public void testGetProviderName() throws IOException {
		ProviderRecord proRecord = new ProviderRecord();
		proRecord.createProvider("tanner","123 Alabama Blvd");
		assertEquals("tanner",proRecord.getProviderName("100000000"));
	}

	@Test // Test Failure
	public void testUpdateProvider() throws IOException {
		ProviderRecord proRecord = new ProviderRecord();
		proRecord.createProvider("tanner","123 Alabama Blvd");
		proRecord.updateProvider("100000000","blake","1234 Alabama Blvd");
		assertNotEquals("tanner", proRecord.getProviderName("100000000"));
	}

	@Test // Test Success
	public void testVerifyProvider() throws IOException {
		ProviderRecord proRecord = new ProviderRecord();
		proRecord.createProvider("tanner","123 Alabama Blvd");
		assertEquals(false,proRecord.verifyProvider("10000000"));
	}
}
