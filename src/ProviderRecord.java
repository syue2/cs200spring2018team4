package project4;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.ArrayList;

/**
 * Class {@code ProviderRecord} contains all existing providers
 * @author Coleman Cost
 */
public class ProviderRecord {
	private ArrayList<Provider> providerList = new ArrayList<Provider>();
	private Path providerFile;
	private int nextNumberInt;
	private String nextNumber;
	private int size;
	
	/**
	 * Constructor {@code ProviderRecord()} initializes variables to be used in creating new providers.
	 */
	public ProviderRecord() {
		size = 0;
		nextNumberInt = 100000000;
		nextNumber = String.valueOf(nextNumberInt);
	}
	
	/**
	 * Getter {@code getProviderName(String)} retrieves the name of a provider.
	 * @param providerNumber this is the password
	 * @return name of a provider
	 */
	public String getProviderName(String providerNumber) {
		int i;
		for (i = 0; i < size; i++) {
			if (providerNumber.equals(providerList.get(i).getProviderNumber())) {
				break;
			}
		}
		return providerList.get(i).getProviderName();
	}
	
	/**
	 * Getter {@code getProviderAddress(String)} retrieves the address of a provider.
	 * @param providerNumber this is the password
	 * @return address of a provider
	 */
	public String getProviderAddress(String providerNumber) {
		int i;
		for (i = 0; i < size; i++) {
			if (providerNumber.equals(providerList.get(i).getProviderNumber())) {
				break;
			}
		}
		return providerList.get(i).getProviderAddress();
	}
	
	/**
	 * Method {@code deleteFile()} deletes the file.
	 * @return Nothing.
	 * @throws IOException
	 */
	public void deleteFile() throws IOException {
		Path files;
		files = Paths.get("provider_record.txt");
		Files.delete(files);
		
		return;
	}
	
	/**
	 * Method {@code verifyProvider(String)} this provides a login functionality for providers.
	 * @param providerNumber this is the password
	 * @return true if verified, false if password is incorrect
	 */
	public boolean verifyProvider(String providerNumber) { //NEED TO FIX VERIFY PROVIDER
		int j = -1;
		int i;
		for (i = 0; i < size; i++) {
			if (providerNumber.equals(providerList.get(i).getProviderNumber())) {
				j = i;
				break;
			}
		}
		
		if (j >= 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Method {@code createProvider(String,String) makes a new provider.
	 * @param name    name for provider
	 * @param address address for provider
	 * @throws IOException 
	 */
	public void createProvider(String name, String address) throws IOException{
		Provider newProvider = new Provider(nextNumber,name,address);
		providerList.add(newProvider);
		size = size + 1;
		int i;
		providerFile = Paths.get("provider_record.txt"); //MAKES THE FILE
		ArrayList<String> lines = new ArrayList<String>();
		for (i=0; i < size; i++) {
			lines.add(String.valueOf(i+1)+ ". " + providerList.get(i).getProviderName() + " " + providerList.get(i).getProviderNumber() + " " + providerList.get(i).getProviderAddress() + "\n");
	
		}
		Files.write(providerFile, lines);
	
		System.out.println("A provider account for " + name + " has been created.\nThe provider number for this account is " + nextNumber + ".\n");
		
		nextNumberInt = nextNumberInt + 1;
		nextNumber = String.valueOf(nextNumberInt);
		
		return;
	}
	
	/**
	 * Member {@code updateProvider(String,String,String)} updates the provider.
	 * @param number     the password
	 * @param newName    the new name
	 * @param newAddress the new address
	 * @throws IOException 
	 */
	public void updateProvider(String number, String newName, String newAddress) throws IOException {
		int i = -1;
		
		for (i = 0; i < size; i++) {
			if (number.equals(providerList.get(i).getProviderNumber()))
			break;
		}
		providerList.get(i).setProviderName(newName);
		providerList.get(i).setProviderAddress(newAddress);
		System.out.println("Provider has been updated.\n");

		providerFile = Paths.get("provider_record.txt"); //MAKES THE FILE
		ArrayList<String> lines = new ArrayList<String>();
		for (i=0; i < size; i++) {
			lines.add(String.valueOf(i+1)+ ". " + providerList.get(i).getProviderName() + " " + providerList.get(i).getProviderNumber() + " " + providerList.get(i).getProviderAddress() + "\n");
	
		}
		Files.write(providerFile, lines);
		
		return;
	}
	
	/**
	 * Getter {@code deleteProvider(String)} deletes the provider.
	 * @param number password of the provider 
	 * @return Nothing.
	 * @throws IOException 
	 */
	public void deleteProvider(String number) throws IOException {
		int i = -1;
		
		for (i = 0; i < size; i++) {
			if (number.equals(providerList.get(i).getProviderNumber()))
			break;
		}
		providerList.remove(i);
		System.out.println("Provider has been deleted.\n");
		size = size - 1;
		
		providerFile = Paths.get("provider_record.txt"); //MAKES THE FILE
		ArrayList<String> lines = new ArrayList <String>();
		for (i=0; i < size; i++) {
			lines.add(String.valueOf(i+1)+ ". " + providerList.get(i).getProviderName() + " " + providerList.get(i).getProviderNumber() + " " + providerList.get(i).getProviderAddress() + "\n");
	
		}
		Files.write(providerFile, lines);
		return;
	}
}