package project4;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.ArrayList; 

/**
 * Class {@code MemberRecord} writes member information to a file.
 * @author Coleman Cost
 */
public class MemberRecord {
	private ArrayList<Member> memberList = new ArrayList<Member>();
	private Path memberFile;
	private int nextNumberInt;
	private String nextNumber;
	private int size;
	
	/**
	 * Constructor {@code MemberRecord} creates a new {@code MemberRecord}.
	 */
	public MemberRecord() {
		size = 0;
		nextNumberInt = 100000000;
		nextNumber = String.valueOf(nextNumberInt);
	}
	
	/**
	 * Getter {@code getMemberName(String)} retrieves attribute memberName.
	 * @param memberNumber the password of a member
	 * @return the member's name
	 */
	public String getMemberName(String memberNumber) {
		int i;
		for (i = 0; i < size; i++) {
			if (memberNumber.equals(memberList.get(i).getMemberNumber())) {
				break;
			}
		}
		return memberList.get(i).getMemberName();
	}
	
	/**
	 * Getter {@code getMemberAddress(String)} retrieves attribute memberAddress.
	 * @param memberNumber the password of a member
	 * @return the member's address
	 */
	public String getMemberAddress(String memberNumber) {
		int i;
		for (i = 0; i < size; i++) {
			if (memberNumber.equals(memberList.get(i).getMemberNumber())) {
				break;
			}
		}
		return memberList.get(i).getMemberAddress();
	}
	
	/**
	 * Method {@code deleteFile()} deletes the file.
	 * @return Nothing.
	 * @throws IOException
	 */
	public void deleteFile() throws IOException {
		Path files;
		files = Paths.get("member_record.txt");
		Files.delete(files);
		
		return;
	}
	
	/**
	 * Method {@code verifyMember(String)} reflects functionality of a "login" feature.
	 * @param memberNumber this is the password to be verified
	 * @return true if verified, false if not verified
	 */
	public boolean verifyMember(String memberNumber) {
		int j = -1;
		int i;
		for (i = 0; i < size; i++) {
			if (memberNumber.equals(memberList.get(i).getMemberNumber())) {
				j = i;
				break;
			}
		}
		
		if (j >= 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Method {@code createMember(String,String)} creates a new {@code Member} object as well as a file.
	 * @param name attribute to be added
	 * @param address attribute to be added
	 * @return Nothing.
	 */
	public void createMember(String name, String address)throws IOException{
		Member newMember = new Member(nextNumber, name, address);
		memberList.add(newMember);

		System.out.println("A member account for " + name + " has been created.\nThe member number for this account is " + nextNumber + ".\n");
		size = size + 1;
		nextNumberInt = nextNumberInt + 1;
		nextNumber = String.valueOf(nextNumberInt);
		
		int i;
		memberFile = Paths.get("member_record.txt"); //MAKES THE FILE
		ArrayList<String> lines = new ArrayList<String>();
		for (i=0; i < size; i++) {
			lines.add(String.valueOf(i+1)+ ". " + memberList.get(i).getMemberName() + " " + memberList.get(i).getMemberNumber() + " " + memberList.get(i).getMemberAddress() + "\n");
	
		}
		Files.write(memberFile, lines);
		return;
	}
	
	/**
	 * Method {@code updateMember(String,String,String)} allows a change to a member.
	 * @param number this is the member's password
	 * @param newName this is the new name
	 * @param newAddress this is the new address
	 * @return Nothing.
	 */
	public void updateMember(String number, String newName, String newAddress)throws IOException {
		int i = -1;
		
		for (i = 0; i < size; i++) {
			if (number.equals(memberList.get(i).getMemberNumber()))
			break;
		}
		memberList.get(i).setMemberName(newName);
		memberList.get(i).setMemberAddress(newAddress);
		System.out.println("Member has been updated.\n");
		
		memberFile = Paths.get("member_record.txt"); //MAKES THE FILE
		ArrayList<String> lines = new ArrayList<String>();
		for (i=0; i < size; i++) {
			lines.add(String.valueOf(i+1)+ ". " + memberList.get(i).getMemberName() + " " + memberList.get(i).getMemberNumber() + " " + memberList.get(i).getMemberAddress() + "\n");
	
		}
		Files.write(memberFile, lines);
		
		return;
		
	}
	
	/**
	 * Method {@code deleteMember(String)} deletes a member.
	 * @param number password of the member to delete
	 * @return Nothing.
	 * @throws IOException
	 */
	public void deleteMember(String number)throws IOException {
		int i = -1;
		
		for (i = 0; i < size; i++) {
			if (number.equals(memberList.get(i).getMemberNumber()))
			break;
		}
		memberList.remove(i);
		System.out.println("Member has been deleted.\n");
		size = size - 1;
		
		memberFile = Paths.get("member_record.txt"); //MAKES THE FILE
		ArrayList<String> lines = new ArrayList<String>();
		for (i=0; i < size; i++) {
			lines.add(String.valueOf(i+1)+ ". " + memberList.get(i).getMemberName() + " " + memberList.get(i).getMemberNumber() + " " + memberList.get(i).getMemberAddress() + "\n");
	
		}
		Files.write(memberFile, lines);
		return;
	}
	
	
	
	
	
}