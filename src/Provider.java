package project4;
/**
 * Class {@code Provider} contains attributes of each provider
 * @author Blake Pennington
 */
public class Provider {
	private String number;
	private String name;
	private String address;
	
	/**
	 * Constructor Provider(String,String,String) creates a new provider
	 * @param number  this is the password
	 * @param name    this is the name
	 * @param address this is the address
	 */
	public Provider(String number, String name, String address) {
		this.number = number;
		this.name = name;
		this.address = address;
	}
	
	/**
	 * Getter {@code getProviderName()} retrieves a provider's name.
	 * @return name the provider's name
	 */
	public String getProviderName() {
		return name;
	}
	
	/**
	 * Getter {@code getProviderNumber()} retrieves a provider's password.
	 * @return number the provider's password
	 */
	public String getProviderNumber() {
		return number;
	}
	
	/**
	 * Getter {@code getProviderAddress()} retrieves a provider's address.
	 * @return address the provider's address
	 */
	public String getProviderAddress() {
		return address;
	}
	
	/**
	 * Setter {@code setProviderName(String)} updates a provider's name.
	 * @param name the new name for a provider
	 * @return Nothing.
	 */
	public void setProviderName(String name) {
		this.name = name;
		return;
	}
	
	/**
	 * Setter {@code setProviderAddress(String)} updates a provider's address.
	 * @param address the new address for a provider
	 * @return Nothing. 
	 */
	public void setProviderAddress(String address) {
		this.address = address;
		return;
	}
}