# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is the meeting place for members of Team 4 
* Version 1.0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Contribution guidelines ###

* Please make sure no one is working on the same file as you
* Ask the other team members before making commits
* It never hurts to include a commit message that reflects the changes in your commit

### Who do I talk to? ###

* If you have questions, ask Frank on Slack
* Make sure to communicate often with your team members to determine progress and to ensure no miscommunicative errors